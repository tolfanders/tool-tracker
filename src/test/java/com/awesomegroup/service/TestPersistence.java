package com.awesomegroup.service;


import com.awesomegroup.model.*;
import com.awesomegroup.util.Mails;
import com.awesomegroup.util.Settings;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.testng.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.testng.annotations.*;

import javax.inject.Inject;
import javax.mail.*;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Store;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Logger;

import static org.testng.Assert.*;
import static com.awesomegroup.service.SampleData.*;


/**
 * Created by tbruun on 4/28/15.
 */

public class TestPersistence extends Arquillian {

    private static final Logger LOGGER = Logger.getLogger(TestPersistence.class.getName());

    @Deployment
    public static Archive<?> createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                //enums
                .addClasses(
                        Settings.class,
                        Mails.class
                )
                //services
                .addClasses(
                        UserService.class,
                        ToolService.class,
                        GarageService.class,
                        HistoryService.class,
                        MailService.class,
                        SettingService.class
                )
                //models
                .addClasses(User.class,
                        Tool.class,
                        History.class,
                        Garage.class,
                        Setting.class,
                        Category.class)
                //resources
                .addClasses(
                        SampleData.class
                )
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml");
    }

    @Inject
    UserService userService;

    @Inject
    ToolService toolService;

    @Inject
    GarageService garageService;

    @Inject
    MailService mailService;


    @Test(priority = 0)
    public void testServicesNotNull() {
        LOGGER.info(() -> "Verifying that service is not null.");
        //Test that we have a user service
        assertNotNull(userService);
        assertNotNull(toolService);
        assertNotNull(garageService);
    }

    /**
     * Test to ensure that a user registration is possible.
     *
     * @throws Exception
     */
    @Test(priority = 1)
    public void testUserRegistration() throws Exception {
        //by getters/setters
        User user = new User();
        user.setFirstName(USER1_FIRST_NAME);
        user.setLastName(USER1_LAST_NAME);
        user.setUserName(USER1_USERNAME);
        user.setUserType(USER1_USER_TYPE);
        user.setPassword(USER1_PASSWORD);
        user.setEmail(USER1_EMAIL);

        //by constructor
        User user2 = new User(USER2_FIRST_NAME, USER2_LAST_NAME, USER2_PASSWORD, USER2_USERNAME, USER2_USER_TYPE, USER2_EMAIL);

        assertTrue(userService.addUser(user));
        assertTrue(userService.addUser(user2));
    }

    /**
     * Test to ensure that garage registration is possible
     *
     * @throws Exception
     */
    @Test(priority = 2)
    public void testGarageRegistration() throws Exception {
        Garage garage = new Garage();
        garage.setGarageName(GARAGE_NAME);
        garage.setCreated(GARAGE_TIME_CREATED);
        garage.setLocation(GARAGE_LOCATION);

        Garage persistedGarage = garageService.addGarage(garage);

        assertEquals(persistedGarage.getGarageName(), GARAGE_NAME);
        assertEquals(persistedGarage.getCreated(), GARAGE_TIME_CREATED);
        assertEquals(persistedGarage.getLocation(), GARAGE_LOCATION);
    }

    /**
     * Test to ensure that tool registration is possible.
     *
     * @throws Exception
     */
    @Test(priority = 3)
    public void testToolRegistration() throws Exception {
        Tool tool = new Tool();
        tool.setToolName(TOOL_NAME);
        tool.setCategory(TOOL_CAT);
        tool.setDescription(TOOL_DESC);
        tool.setNotes(TOOL_NOTES);

        Garage garage = garageService.findGarageByName(GARAGE_NAME);

        Tool persistedTool = toolService.addTool(tool, garage);

        assertTrue(persistedTool.getId() >= 1);
        assertEquals(persistedTool.getCategory(), TOOL_CAT);
        assertEquals(persistedTool.getDescription(), TOOL_DESC);
        assertEquals(persistedTool.getNotes(), TOOL_NOTES);
        assertEquals(persistedTool.getToolName(), TOOL_NAME);
        assertNull(persistedTool.getHistoryList());
    }

    /**
     * Test to ensure that relationship between tool and user works as intended.
     */
    @Test(priority = 4)
    public void testAddToolToUser() {
        Tool tool = toolService.findAll().get(0);
        User user = userService.findByUsername(USER1_USERNAME);

        toolService.takeTool(user, tool.getId());

        Tool persistedTool = toolService.findByToolID(tool.getId());
        assertNotNull(persistedTool.getUser());
    }

    /**
     * Test to verify that user we get from tool, is in fact the same user that we created.
     */
    @Test(priority = 5)
    public void testVerifyUserFromTool() {
        Tool tool = toolService.findAll().get(0);
        User userFromTool = tool.getUser();
        assertTrue(userFromTool.getId() >= 1);
        assertEquals(userFromTool.getFirstName(), USER1_FIRST_NAME);
        assertEquals(userFromTool.getLastName(), USER1_LAST_NAME);
        assertEquals(userFromTool.getUserName(), USER1_USERNAME);
        assertEquals(userFromTool.getPassword(), USER1_PASSWORD);
        assertEquals(userFromTool.getEmail(), USER1_EMAIL);
    }

    /**
     * Test to verify that it's possible to checkout a tool from one user to another.
     *
     * @throws Exception
     */
    @Test(priority = 6)
    public void testCheckoutTool() throws Exception {

        User persistedUser2 = userService.findByUsername(USER2_USERNAME);
        Tool persistedTool = toolService.findAll().get(0);

        // Check if tool can transfer from previous user to the new one
        Tool transferedFromOriginTool = toolService.takeTool(persistedUser2, persistedTool.getId());
        assertTrue(transferedFromOriginTool.getId() >= 1);
        assertEquals(transferedFromOriginTool.getToolName(), TOOL_NAME);
        assertEquals(transferedFromOriginTool.getCategory(), TOOL_CAT);
        assertEquals(transferedFromOriginTool.getNotes(), TOOL_NOTES);
        assertEquals(transferedFromOriginTool.getDescription(), TOOL_DESC);

    }

    // Check if tool can get checked back to its garage
    @Test(priority = 7)
    public void testCheckInTool() throws Exception {
        User persistedUser = userService.findByUsername(USER2_USERNAME);
        Garage persistedGarage = garageService.findGarageByName(GARAGE_NAME);
        List<Tool> persistedTool = toolService.findAll();

        //Check if tool can get stored back to the garage
        Tool transferedToolBackToGarage = toolService.checkInTool(persistedUser, persistedTool.get(0).getId(), persistedGarage.getId());
        assertNotNull(transferedToolBackToGarage);
        assertNull(transferedToolBackToGarage.getUser());
    }


    /**
     * Test to ensure that history is added to the history list as we have transfered the tool.
     * @throws Exception
     */
    @Test(priority = 8)
    public void testHistoryIsUpdated() throws Exception {
        Tool tool = toolService.findAll().get(0);
        assertNotNull(tool.getHistoryList());
    }

    /**
     * Test to verify that it is possible to break a relation, and remove the tool from the database.
     *
     * @throws Exception
     */
    @Test(priority = 9)
    public void testRemoveTool() throws Exception {
        Tool tool = toolService.findAll().get(0);
        toolService.removeTool(tool);
        Tool removedTool = toolService.findByToolID(tool.getId());
        assertEquals(removedTool.getStillAlive(), 0);
    }

    /**
     * Test to verify that it is possible to to remove a user from the database.
     *
     * @throws Exception
     */
    @Test(priority = 10)
    public void testRemoveUser() throws Exception {
        User persistedUser = userService.findByUsername(USER1_USERNAME);
        userService.removeUser(persistedUser.getId());
        assertNull(userService.findByUserID(persistedUser.getId()));
    }

    @Test(priority = 11)
    public void testMailSender() throws Exception {
        Random generator = new Random();
        int i = generator.nextInt(100);

        String testMessage = "Integration test message: " + i;

        mailService.sendMail("lolautobuilder@gmail.com", "Integration test message", testMessage);

        //for some reason gmail adds a newline to the message, making test fail.
        //need to remove this newline before reading string
        String expectedResult = readMail()
                .replace("\n", "")
                .replace("\r", "");

        assertEquals(expectedResult, testMessage);
    }


    /**
     * This method reads to first row of the last recieved message on lolautobuilder account.
     * @return email message as String.
     */
    public String readMail() {
        //add protocol to properties
        Properties props = new Properties();
        props.setProperty("mail.store.protocol", "imaps");
        String message = null;

        try {
            //get session
            Session session = Session.getInstance(props, null);
            Store store = session.getStore();
            store.connect("imap.gmail.com", "lolautobuilder@gmail.com", "guide2JSON");

            //open folder
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);

            //get message
            Message msg = inbox.getMessage(inbox.getMessageCount());

            //read content from message
            message = (String) msg.getContent();

            //delete message
            msg.setFlag(Flags.Flag.DELETED, true);
            inbox.close(true);
        } catch (Exception mex) {
            mex.printStackTrace();
        }
        return message;
    }
}