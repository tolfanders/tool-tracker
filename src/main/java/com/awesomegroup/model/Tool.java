package com.awesomegroup.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Entity for a Tool <p>
 *
 * A Tool is the main rockstar of this domain. <p>
 * This entity represents a real life tool and is transferable between users and/or garages.
 *
 * Even if a tool have a user, it will still have a garage, so the only way to know for sure that a tool is in a garage
 * is to check if user is null. The reason behind this is to know where the tool belongs, even if its still with a user.
 */
@Entity
@Table(name = "tool")

    /* -------------- */
    /* Entity Queries */
    /* -------------- */

@NamedQueries({
        @NamedQuery(name = "Tool.findAll",
                query = "SELECT DISTINCT  t FROM Tool t where t.stillAlive = 1"),

        @NamedQuery(name = "Tool.findByName",
                query = "SELECT t FROM Tool t WHERE t.toolName = :toolName"),

        @NamedQuery(name = "Tool.findByCategory",
                query = "SELECT t FROM Tool t WHERE t.category = :category"),

        @NamedQuery(name = "Tool.findById",
                query = "SELECT t FROM Tool t WHERE t.id =:id"),

        @NamedQuery(name = "Tool.findByUsername",
                query = "SELECT t FROM Tool t WHERE t.user.id = :userId"),

        @NamedQuery(name = "Tool.findByGarage",
                query = "SELECT t FROM Tool t WHERE t.garage.id = :garageId")
})
/**
 * Named entity graphs from JPA 2.1 to trigger eager fetching of various relations when needed at runtime.
 */
@NamedEntityGraphs({
        @NamedEntityGraph(
        name = "ToolFetchAll",
        attributeNodes = {
                @NamedAttributeNode("historyList"),
                @NamedAttributeNode("garage"),
                @NamedAttributeNode("user")
        }),
        @NamedEntityGraph(
                name = "ToolFetchForGarageDetail",
                attributeNodes = {
                        @NamedAttributeNode("garage"),
                        @NamedAttributeNode("user")
                }
        )
}
)
public class Tool implements Serializable{

    /* ------------ */
    /* Local fields */
    /* ------------ */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "tool_name")
    private String toolName;

    @NotNull
    private String category;

    private String description;

    private String notes;

    @Column(name = "still_alive")
    private int stillAlive = 1;


    /* --------------- */
    /* Relation fields */
    /* --------------- */

    @ManyToOne()
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToMany(mappedBy = "tool")
    private List<History> historyList;

    @ManyToOne
    @JoinColumn(name = "garage_id", referencedColumnName = "id")
    private Garage garage;


    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setasId(int id) {
        this.id = id;
    }

    public String getToolName() {
        return toolName;
    }

    public void setToolName(String toolName) {
        this.toolName = toolName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getStillAlive() {
        return stillAlive;
    }

    public void setStillAlive(int stillAlive) {
        this.stillAlive = stillAlive;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Garage getGarage() {
        return garage;
    }

    public void setGarage(Garage garage) {
        this.garage = garage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tool tool = (Tool) o;
        return Objects.equals(id, tool.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}