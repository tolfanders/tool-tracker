package com.awesomegroup.model;

import com.awesomegroup.util.Settings;

import javax.persistence.*;

/**
 * Entity for a Setting <p>
 *
 * A Setting represent a key/value setting specific for a User. <p>
 *
 * This could be any setting, but key value needs to come from the {@code Setting} Enum.<br>
 * For more information on how to work with Settings in this project read further at the SettingService documentation.
 */
@Entity
@Table(name = "setting")
public class Setting {

    public Setting() {}

    public Setting(Settings key, String value) {
        this.key = key;
        this.value = value;
    }

    /* ------------ */
    /* Local fields */
    /* ------------ */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "setting_key")
    @Enumerated(EnumType.STRING)
    private Settings key;

    @Column(name = "setting_value")
    private String value;

    /* --------------- */
    /* Relation fields */
    /* --------------- */

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;


    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Settings getKey() {
        return key;
    }

    public void setKey(Settings key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
