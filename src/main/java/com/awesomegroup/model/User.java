package com.awesomegroup.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * Entity for a User <p>
 *
 * A user is used both for authentication to the web application and to store all data related to the user. <p>
 *
 */
@Entity
@Table(name = "user")

    /* -------------- */
    /* Entity Queries */
    /* -------------- */

@NamedQueries({
        @NamedQuery(name = "User.findAll",
                query = "SELECT u FROM User u WHERE u.stillAlive = 1"),
        @NamedQuery(name = "User.findByUserName",
                query = "SELECT u FROM User u WHERE u.stillAlive = 1 AND u.userName = :userName"),
        @NamedQuery(name = "User.findByBinaryUsername",
                query = "SELECT u FROM User u WHERE u.stillAlive = 1 AND  u.userName LIKE :userName")
})
public class User implements Serializable {

    /* ------------ */
    /* Local fields */
    /* ------------ */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    private String password;

    @NotNull
    @Column(name = "user_name")
    private String userName;

    @NotNull
    @Column(name = "user_type")
    private String userType;

    @NotNull
    @Column(name = "still_alive")
    private int stillAlive = 1;

    @NotNull
    private String email;

    private String notes;

    /* --------------- */
    /* Relation fields */
    /* --------------- */

    @OneToMany(mappedBy = "user")
    private List<Tool> tools;

    @OneToMany(mappedBy = "user")
    private List<History> historyList;

    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
    private List<Setting> settings;

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public User() {
    }

    public User(String firstName, String lastName, String password, String userName, String userType, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.userName = userName;
        this.userType = userType;
        this.email = email;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<Tool> getTools() {
        return tools;
    }

    public void setTools(List<Tool> tools) {
        this.tools = tools;
    }

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }

    public int getStillAlive() {
        return stillAlive;
    }

    public void setStillAlive(int stillAlive) {
        this.stillAlive = stillAlive;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Setting> getSettings() {
        return settings;
    }

    public void setSettings(List<Setting> settings) {
        this.settings = settings;
    }
}