package com.awesomegroup.model;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Entity for a History <p>
 *
 * A History object represents that an "tool event" has taken place. This can be either a tool being taken from a garage,
 * or that a tool is taken over from one user to another. Deliveries back to garage is also tracked by history, but
 * a new event is not created.<p>
 *
 * So how to we know the difference if a tool is at user or at garage ? <br>
 * When user takes responsibility for a tool, a new History event is added. But checked_in field has a value of
 * {@code null}. <p>
 *
 * When User chooses to deliver a tool back to garage or another user takes over responsibility for a tool, the
 * checked_in field gets a proper timestamp.
 *
 */
@NamedQueries({
        @NamedQuery(name = "History.findOldTools",
                query = "SELECT DISTINCT h FROM History h WHERE WEEK(h.checkedOut) <= WEEK(current_date())-12 AND h.checkedIn IS NULL")
})
@Entity
@Table(name = "history")
public class History {

    /* ------------ */
    /* Local fields */
    /* ------------ */

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "checked_out")
    private Timestamp checkedOut;

    @Column(name = "checked_in")
    private Timestamp checkedIn;

    /* --------------- */
    /* Relation fields */
    /* --------------- */

    @ManyToOne
    @JoinColumn(name = "tool_id", referencedColumnName = "id")
    private Tool tool;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;


    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(Timestamp checkedOut) {
        this.checkedOut = checkedOut;
    }

    public Tool getTool() {
        return tool;
    }

    public void setTool(Tool tool) {
        this.tool = tool;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timestamp getCheckedIn() {
        return checkedIn;
    }

    public void setCheckedIn(Timestamp checkedIn) {
        this.checkedIn = checkedIn;
    }
}
