package com.awesomegroup.service;

import com.awesomegroup.model.Category;
import com.awesomegroup.model.Garage;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.List;

/**
 * Service handling requests concerning {@code Tool} entities. <p>
 *
 * For further reading about a Tool, read {@link Tool} documentation.
 */
@Stateless
public class ToolService implements Serializable {

    @PersistenceContext(unitName = "primary")
    protected EntityManager em;

    /**
     * Finds all tools in database.
     * @return List of all tools in database, or null if no tools were found.
     */
    public List<Tool> findAll() {
        List<Tool> tools;
        try {
            tools = em.createQuery("SELECT t FROM Tool t WHERE t.stillAlive=1", Tool.class).getResultList();
        } catch (NoResultException ex) {
            tools = null;
        }
        return tools;
    }

    /**
     * Finds all tools in a given garage.
     *
     * @param garageId id of the garage you want to find tools from.
     * @return List of all tools in garage, or null if no tools were found.
     */
    public List<Tool> findToolsInGarage(int garageId) {
        Garage garage;
        try {
            garage = em.createNamedQuery("Garage.findGarageById", Garage.class)
                    .setParameter("id", garageId)
                    .setHint("javax.persistence.fetchgraph", em.getEntityGraph("FetchGarageWithToolAndUser"))
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
        return garage.getTools();
    }

    /**
     * Finds all tools in database with all relations loaded at query time.
     * @return List of all tools in database with loaded realations, or null if no tools were found.
     */
    public List<Tool> findAllDeep() {
        List<Tool> tools;
        try {
            tools = em.createNamedQuery("Tool.findAll", Tool.class)
                    .setHint("javax.persistence.fetchgraph", em.getEntityGraph("ToolFetchAll"))
                    .getResultList();
        } catch (NoResultException ex) {
            tools = null;
        }
        return tools;
    }

    /**
     * Finds all tools associated with a user.
     *
     * @param userId id of the user you want to find tools from.
     * @return List of all tools from user, or null if no tools were found.
     */
    public List<Tool> findAllForUser(int userId) {
        List<Tool> tools;
        try {
            tools = em.createNamedQuery("Tool.findByUsername", Tool.class)
                    .setParameter("userId", userId)
                    .getResultList();
        } catch (NoResultException ex) {
            tools = null;
        }
        return tools;
    }

    /**
     * Finds a tool from its id.
     *
     * @param toolID id tool from.
     * @return Tool from id, or null if no tool were found.
     */
    public Tool findByToolID(int toolID) {
        Tool tool;
        try {
            tool = em.find(Tool.class, toolID);
        } catch (NoResultException ex) {
            tool = null;
        }
        return tool;
    }

    /**
     * This method should be called whenever a user want to take responsibility over a tool.
     *
     * @param destinationUser user that wants to take over tool.
     * @param toolId id of tool to take over.
     * @return tool taken over by user, or null if no tool were found.
     */
    public Tool takeTool(User destinationUser, int toolId) {
        Tool tool;

        try {
            tool = em.find(Tool.class, toolId);
            destinationUser = em.find(User.class, destinationUser.getId());
        } catch (NoResultException e) {
            return null;
        }

        tool.setUser(destinationUser);
        tool = em.merge(tool);

        return tool;
    }

    /**
     * This method should be called whenever a user want to return a tool.
     *
     * @param user user that has a tool he wants to check in.
     * @param toolId id of the tool he wants to check in.
     * @param garageId garage id to witch garage he wants to put it to.
     * @return Tool returned by user, or null if no tool were found
     */
    public Tool checkInTool(User user, int toolId, int garageId) {

        user = em.find(User.class, user.getId());
        Tool tool = em.find(Tool.class, toolId);
        Garage garage = em.find(Garage.class, garageId);

        if (user != null && user.getTools().contains(tool)) {
            transferTool(garage, tool);
        } else {
            tool = null;
        }
        return tool;
    }

    /**
     * Methd that transfers a to a garage.<br>
     * Developer should not care about this method, and instead call {@code checkInTool()} for his tool check-in-needs.
     * @param destinationGarage garage the tool should be transferred to.
     * @param tool tool to transfer.
     * @return the tool that has been transferred.
     */
    private Tool transferTool(Garage destinationGarage, Tool tool) {
        tool.setUser(null);
        tool.setGarage(destinationGarage);

        return tool;
    }

    /**
     * Performs a "soft-delete" on a tool, making it invisible for further use in this application.
     * @param tool tool to delete.
     */
    public void removeTool(Tool tool) {
        Tool toRemove = em.find(Tool.class, tool.getId());
        toRemove.setStillAlive(0);
    }

    /**
     * Performes a update on the tool.<br>
     * Use this when working with a detached entity, and you want to perform a update to the database on it.
     * @param tool tool to update.
     * @return true if success, false if failure.
     */
    public boolean updateTool(Tool tool) {
        boolean success = false;
        tool = em.merge(tool);

        if (tool != null) {
            success = true;
        }

        return success;
    }

    /**
     * Adds a tool to the database.
     * @param tool Tool to add.
     * @param garage default garage for tool.
     * @return newly added tool, with its brand new shining id.
     */
    public Tool addTool(Tool tool, Garage garage) {
        tool.setGarage(em.merge(garage));
        em.persist(tool);

        return tool;
    }

    /**
     * Finds a single tool by its name.
     * @param toolName name to search for.
     * @return Tool found.
     */
    public Tool findByToolName(String toolName){
        Tool tool;
        try {
            tool = em.createNamedQuery("Tool.findByName", Tool.class)
                    .setParameter("toolName", toolName)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
        return tool;
    }

    /**
     * Gives a list of all available categories..
     * @return list of all categories, or null if no categories were found.
     */
    public List<Category> getCategories() {
        List<Category> categories;
        try {
            categories = em.createNamedQuery("Category.findAll", Category.class)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return categories;
    }

    /**
     * gives a list of categories matching the provided name.
     * @param name name to search.
     * @return list of categories matching the provided name, or null if no results.
     */
    public List<Category> getCategoryByName(String name) {
        List<Category> categories;
        try {
            categories = em.createNamedQuery("Category.findByName", Category.class)
                    .setParameter("category", name)
                    .getResultList();
        } catch (NoResultException e) {
            return null;
        }
        return categories;
    }

    /**
     * Creates a new Category in database.
     * @param categoryName category name.
     */
    public void createCategory(String categoryName){
        Category category = new Category();
        category.setCategory(categoryName);
        em.persist(category);
    }
}
