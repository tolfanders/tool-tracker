package com.awesomegroup.util;

/**
 * Enum storing Message constants.
 * This for easier messaging handling.
 */
public enum MessageType {

    /* --------------- */
    /* Constant fields */
    /* --------------- */

    NOTIFICATION_SUCCESS("notification-message-success"),
    NOTIFICATION_ERROR("notification-message-error"),
    PROMPT_SUCCESS("prompt-message-success"),
    PROMPT_ERROR("prompt-message-error"),
    PROMPT_DEFAULT("prompt-message-default"),
    DIALOG_DEFAULT("dialog-message-default"),
    DIALOG_ERROR("dialog-message-error"),
    DIALOG_SUCCESS("dialog-message-success");

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    private String value;

    MessageType(String value){
        this.value = value;
    }


    /* ----------------- */
    /* Setters & Getters */
    /* ----------------- */

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
