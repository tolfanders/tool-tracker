package com.awesomegroup.util;


import java.util.UUID;

/**
 * Utility class that generates a random GUID/UUID
 */
public class Guids {


    /**
     * Promises to generate a random GUID/UUID
     * @return GUID/UUID
     */
    public static String makeGuid() {

        UUID guid = UUID.randomUUID();
        return guid.toString();
    }
}
