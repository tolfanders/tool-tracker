package com.awesomegroup.beans;

import com.awesomegroup.model.Garage;
import com.awesomegroup.model.Setting;
import com.awesomegroup.model.Tool;
import com.awesomegroup.model.User;
import com.awesomegroup.service.*;
import com.awesomegroup.util.MessageType;
import com.awesomegroup.util.PasswordHash;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import static com.awesomegroup.util.Settings.RECIEVE_WEEKLY_MAIL;

/**
 * The main backing bean for handling requests concerning the logged in user.
 * This bean is SessionScoped to keep the logged in users information through the whole session.
 */
@Named("sessionBean")
@SessionScoped
public class SessionBean implements Serializable {

    /* ------------ */
    /* Dependencies */
    /* ------------ */

    @Inject
    private UserService userService;
    @Inject
    private GarageService garageService;
    @Inject
    private NavigationBean navigationBean;
    @Inject
    private HistoryService historyService;
    @Inject
    private ToolService toolService;
    @Inject
    private MessageBean messageBean;
    @Inject
    private SettingService settingService;

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    public static final long serialVersionUID = 1L;
    private User sessionUser = new User();      //This is the logged in user. Must be instantiated to avoid NPE.
    private List<Tool> userTools;               //Tools currently checkout by user.
    private Integer toolCheckoutId;
    private Tool toolStagedForTake;
    private String[] passwords = new String[3]; //When change passwords they are stored here while validating
    private List<Garage> garageList;            //List of all garages currently available
    private Garage stageGarageForTakeout;       //Default garage of a selected tool
    private Tool stageToolForTakeout;           //Currently selected tool to return
    private boolean sendWeeklyMail = true;

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * Validate users login credentials.
     * @return a {@code String} which will navigate user to home.jsf, granted he successfully logged in. Else he
     * stays on login page.
     */
    public String login() {
        sessionUser = userService.userLogin(sessionUser.getUserName(), sessionUser.getPassword());
        HttpSession session = Util.getSession();

        if (sessionUser == null) {
            messageBean.createNotificationMessage(
                    MessageType.NOTIFICATION_ERROR,
                    "Failed to log in!",
                    "Either the username or password was invalid");
            session.invalidate();
            return null; // if we redirect the message gets lost in ajax execute. so returning null.
        }

        userTools = toolService.findAllForUser(sessionUser.getId());


        session.setAttribute("username", sessionUser.getUserName());
        session.setAttribute("usertype", sessionUser.getUserType());

        if (sessionUser.getUserType().equals("admin"))
            updateMailSetting();

        updateToolList();
        updateGarageList();
        return navigationBean.toHome();
    }

    /**
     * User takes a tool from a garage, or another user. Calls service method and transfers the tool into logged
     * in users inventory of {@code Tool}s.
     */
    public void checkOutTool() {
        Tool tool = toolService.takeTool(sessionUser, toolStagedForTake.getId());
        if (tool != null) {
            historyService.updateHistory(tool, sessionUser);
        }
        invalidateStagedTool();
        updateUserTools();
        updateGarageList();
        updateToolList();
    }

    /**
     * Placeholder method for being able to have a diverse front-end. Overloaded with and without params.
     */
    public void checkInTool() {
        checkInTool(stageGarageForTakeout.getId(), stageToolForTakeout.getId());
    }

    /**
     * Return selected tool to selected garage. Also calls on methods to make sure {@code Tool}-lists are updated.
     * @param garageId the primary key, ID, of the garage to which the tool will be returned.
     * @param toolId   the primary key, ID, of the tool to return.
     */
    public void checkInTool(int garageId, int toolId) {

        Tool tool = toolService.checkInTool(sessionUser, toolId, garageId);

        if (tool != null) {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS, "Success!", "You returned tool: " + tool.getToolName());
            historyService.updateHistory(tool);
        } else {
            messageBean.createNotificationMessage(MessageType.NOTIFICATION_ERROR, "Failed!", "Oops something went wrong!");
        }
        updateUserTools();
        updateToolList();

    }

    /**
     * Invalidates the users session. He will no longer be able to pass through the filter.
     * @return navigational string to the login page.
     */
    public String logout() {
        HttpSession session = Util.getSession();
        session.invalidate();
        return navigationBean.toLogin();
    }

    /**
     * Calls on service method for updating users password, displays the proper message depending on success.
     */
    public void changePassword() {
        try {
            if (PasswordHash.validatePassword(passwords[0], sessionUser.getPassword())) {
                if (passwords[1].equals(passwords[2])) {
                    sessionUser.setPassword(PasswordHash.createHash(passwords[1]));
                    userService.updateUser(sessionUser);
                    messageBean.createNotificationMessage(MessageType.NOTIFICATION_SUCCESS, "Success!", "Password changed!");
                } else {
                    messageBean.createNotificationMessage(MessageType.NOTIFICATION_ERROR, "Failure!", "New passwords do not match!");
                }
            } else {
                messageBean.createNotificationMessage(MessageType.NOTIFICATION_ERROR, "Failure!", "Wrong password!");
            }
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates users tools with the help of service method.
     */
    public void updateUserTools() {
        userTools = toolService.findAllForUser(sessionUser.getId());

    }

    /**
     * Updates list of all garages.
     */
    public void updateGarageList() {
        garageList = garageService.getAllGarages();
    }

    /**
     * Invalidate the staged tool, used for displaying prompt when taking a tool.
     */
    public void invalidateStagedTool() {
        toolStagedForTake = null;
        toolCheckoutId = null;
    }

    /**
     * Makes sure that the first {@code Tool} in drop down of users list of {@code Tool}s  is properly displayed.
     */
    public void updateToolList() {
        if (!userTools.isEmpty()) {
            stageToolForTakeout = userTools.get(0);
            toolSelectedForReturn();
        }
    }

    /**
     * Selects the default {@code Garage} of selected {@code Tool}, in drop down of available {@code Garage}s.
     */
    public void toolSelectedForReturn() {

        for (Tool t : userTools) {
            if (t.getToolName().equals(stageToolForTakeout.getToolName())) {
                stageToolForTakeout = t;
            }
        }
        if (!userTools.isEmpty()) {
            stageGarageForTakeout = stageToolForTakeout.getGarage();
        }
    }

    /**
     * Is always called by inputswitch ajax listener.
     * updates database with the new boolean.
     */
    public void updateMailSetting() {
        System.out.println("updatemailsetting körs");
        Setting s = settingService.getSettingForUser(RECIEVE_WEEKLY_MAIL, sessionUser);
        if (s == null) {
            settingService.addSettingToUser(RECIEVE_WEEKLY_MAIL, "true", sessionUser);
            sendWeeklyMail = true;
        } else {
            settingService.updateSetting(String.valueOf(sendWeeklyMail), s);
        }
    }

    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */

    public User getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(User sessionUser) {
        this.sessionUser = sessionUser;
    }

    public List<Tool> getUserTools() {
        return userTools;
    }

    public void setUserTools(List<Tool> userTools) {
        this.userTools = userTools;
    }

    public String[] getPasswords() {
        return passwords;
    }

    public void setPasswords(String[] passwords) {
        this.passwords = passwords;
    }

    public Tool getToolStagedForTake() {
        return toolStagedForTake;
    }

    public void setToolStagedForTake(Tool toolStagedForTake) {
        this.toolStagedForTake = toolStagedForTake;
    }

    public void setToolStagedForTake() {
        this.toolStagedForTake = toolService.findByToolID(toolCheckoutId);
    }

    public void setToolStagedForTake(String toolId) {
        int toolIdint;
        try {
            toolIdint = Integer.valueOf(toolId);
            this.toolStagedForTake = toolService.findByToolID(toolIdint);
        } catch (NumberFormatException e) {
            this.toolStagedForTake = null;
        }
    }

    public Integer getToolCheckoutId() {
        return toolCheckoutId;
    }

    public void setToolCheckoutId(Integer toolCheckoutId) {
        this.toolCheckoutId = toolCheckoutId;
    }

    public boolean isSendWeeklyMail() {
        return sendWeeklyMail;
    }

    public List<Garage> getGarageList() {
        return garageList;
    }

    public void setGarageList(List<Garage> garageList) {
        this.garageList = garageList;
    }

    public Garage getStageGarageForTakeout() {
        return stageGarageForTakeout;
    }

    public void setStageGarageForTakeout(Garage stageGarageForTakeout) {
        this.stageGarageForTakeout = stageGarageForTakeout;
    }

    public Tool getStageToolForTakeout() {
        return stageToolForTakeout;
    }

    public void setStageToolForTakeout(Tool stageToolForTakeout) {
        this.stageToolForTakeout = stageToolForTakeout;
    }

    /**
     * This setter method is a ugly hack to circumvent a bug in primefaces that makes inputswitch always send
     * "false" to the setter, so we treat is as a toggle button and just switch the instance boolean around
     * at every call.
     *
     * @param sendWeeklyMail
     */
    public void setSendWeeklyMail(boolean sendWeeklyMail) {
        System.out.println("setSendWeeklyMail körs");
        this.sendWeeklyMail = !this.sendWeeklyMail;
    }

}
