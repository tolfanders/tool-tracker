package com.awesomegroup.beans;

import com.awesomegroup.util.MessageType;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Backing bean for relaying information to the end user.
 */
@Named("messageBean")
@RequestScoped
public class MessageBean {

    /* --------------- */
    /* Instance fields */
    /* --------------- */

    public static final long serialVersionUID = 1L;

    /**
     * These are the components of a message.
     * There are two kind of messages. Notifications and prompts.
     */
    private String notificationType;
    private String notificationHeader;
    private String notificationMessage;

    private String promptType = MessageType.PROMPT_DEFAULT.getValue();
    private String promptHeader;
    private String promptMessage;

    /* ------- */
    /* Methods */
    /* ------- */

    /**
     * Builds a notification message. These fields are read from in our default template, setting values to
     * HTML-tags and thus comprising a message.
     *
     * @param notificationType    Type of message.
     * @param notificationHeader  The header of the message.
     * @param notificationMessage Main information of the message.
     */
    public void createNotificationMessage(MessageType notificationType, String notificationHeader, String notificationMessage) {
        this.notificationType = notificationType.getValue();
        this.notificationMessage = notificationMessage;
        this.notificationHeader = notificationHeader;
    }

    /**
     * Builds a notification message. These fields are read from in our default template, setting values to
     * HTML-tags and thus comprising a message.
     *
     * @param promptType    Type of message.
     * @param promptHeader  The header of the message.
     * @param promptMessage Main information of the message.
     */
    public void createPromptMessage(MessageType promptType, String promptHeader, String promptMessage) {
        this.promptType = promptType.getValue();
        this.promptHeader = promptHeader;
        this.promptMessage = promptMessage;
    }

    /**
     * Hides the prompt message.
     */
    public void closePromptMessage() {
        this.promptType = MessageType.PROMPT_DEFAULT.getValue();
    }


    /* ----------------- */
    /* Getters / Setters */
    /* ----------------- */


    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getNotificationHeader() {
        return notificationHeader;
    }

    public void setNotificationHeader(String notificationHeader) {
        this.notificationHeader = notificationHeader;
    }

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getPromptType() {
        return promptType;
    }

    public void setPromptType(String promptType) {
        this.promptType = promptType;
    }

    public String getPromptHeader() {
        return promptHeader;
    }

    public void setPromptHeader(String promptHeader) {
        this.promptHeader = promptHeader;
    }

    public String getPromptMessage() {
        return promptMessage;
    }

    public void setPromptMessage(String promptMessage) {
        this.promptMessage = promptMessage;
    }
}
