package com.awesomegroup.beans;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Bean for getting navigational strings with parameters.
 */
@Named
@RequestScoped
public class NavigationBean implements Serializable{

    public String toHome(){
        return "home?faces-redirect=true";
    }
    public String toLogin(){return "login?faces-redirect=true";}
    public String toToolDetails(){return "tooldetails?faces-redirect=true";}
    public String toGarageDetails(){return "garagedetails?faces-redirect=true";}
    public String toUserDetails(){return "userdetails?faces-redirect=true";}
    public String toTools(){return "/tools?faces-redirect=true";}
    public String toGarage(){return "/garage?faces-redirect=true";}
    public String toUsers(){return "/user?faces-redirect=true";}
    public String toSettings(){return "/settings?faces-redirect=true";}
}
