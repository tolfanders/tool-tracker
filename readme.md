# ToolHub
Setup description.

  - Java EE 7 compatible server eg Wildfly.
  - Datasource must have the following name java:/jdbc/tooltracker
  - You should make sure that you are using the latest version of Java eg Java 8.
  - Make sure you have maven installed on your computer.
  - Don't forget to configure the properties in the pom.xml so it points to your Wildfly server.
  - Type mvn wildfly:deploy in your terminal to deploy the application onto your server.
  - Do source the provided .sql files for database setup.
  - At this time you should have access to test accounts provided in populate.sql.
  - You should then create your own admin account and remove any unnecessary data in populate.sql.
  - Now you can login to your own admin account and create accounts for all desired users and admins. 
<br/>         
Your application should now be up and running :)
Thank you for choosing Tool Hub as your tool tracking system.
<br/>


> Created by: Daniel Laine, Thomas bruun, Stefan Bahnson och Anders Tolf.